import React, {Component} from 'react';
import axios from 'axios';
import ProductList from "./components/ProductList/ProductList";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    constructor(props) {
        super(props);

        let favouriteFromLSArr=null;
        let inCartFromLSArr=null;
        if (localStorage.Favourites) {
            const favouriteFromLSString = localStorage.getItem('Favourites');
            favouriteFromLSArr = favouriteFromLSString.split(',');
        }
        if (localStorage.getItem('In Cart')) {
            const inCartFromLSString = localStorage.getItem('In Cart');
            inCartFromLSArr = inCartFromLSString.split(',');
        }


        this.state = {
            productList: [],
            favourites: favouriteFromLSArr || [],
            modalIsOpened: false,
            productsInCart:inCartFromLSArr || [],
        };
    }

    toggleModal = (productCode) => {
        this.setState({
            modalIsOpened: !this.state.modalIsOpened,
            goingToAdd:productCode
        })
    };
    closeModal = () => {
        this.setState({modalIsOpened: !this.state.modalIsOpened});
    }

    addToCart = () => {
        const {productsInCart, goingToAdd}=this.state;
        const itemAllreadyInCart = productsInCart.findIndex(productCode => productCode === goingToAdd);

        if (itemAllreadyInCart !== -1) {
            productsInCart.splice(itemAllreadyInCart, 1);
        } else {
            productsInCart.push(goingToAdd);
        }
        localStorage.setItem('In Cart', productsInCart);
        this.closeModal();
    };

    componentDidMount() {
        axios('/product_items.json')
            .then(result => {
                this.setState({
                    productList: result.data
                });
            });
    }

    render() {
        const {productList, favourites, modalIsOpened} = this.state;
        return (
            <>
                <ProductList productList={productList} favourites={favourites} toggleModal={this.toggleModal}/>
                <Modal isOpened={modalIsOpened} header='Cart modal' closeButton={'x'}
                       closeModal={this.closeModal} text='Do you want to add this cute plant to your cart?' actions={[
                    <Button key='doNotAddToCart' classText='btn-inModal btn-inModal__no' text='No' onClick={this.closeModal}/>,
                    <Button key='addToCart' classText='btn-inModal btn-inModal__yes' text='Yes'
                            onClick={this.addToCart}/>
                ]}/>
            </>
        )
    }
}

export default App;
