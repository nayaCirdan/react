import React, {Component} from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends Component {
    render() {
        const {classText, text, onClick} = this.props;
        return (
            <button className={(`btn ${classText}`)} onClick={onClick}>{text}</button>
        );
    }
}

Button.propTypes={
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    classText: PropTypes.string.isRequired
};

export default Button;

