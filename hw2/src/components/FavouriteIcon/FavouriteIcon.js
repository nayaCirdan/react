import React, {Component} from 'react';
import { star } from '../../icons/star'

class FavouriteIcon extends Component {
    render() {
        const {isFavourite, toggleFavorite} = this.props;
        return (
            <div className='icon-container' onClick={toggleFavorite}>
                {star(isFavourite)}
            </div>
        );
    }
}

export default FavouriteIcon;