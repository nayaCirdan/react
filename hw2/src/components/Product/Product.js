import './Product.scss'
import React, {Component} from 'react';
import Button from "../Button/Button";
import FavouriteIcon from "../FavouriteIcon/FavouriteIcon";


class Product extends Component {
    state = {
            isFavourite: false,
        };


    toggleFavorite = () => {
        const {favourites}=this.props;

        const itemFavIndex = favourites.findIndex(productCode => productCode === this.props.vendorCode);

        if (itemFavIndex !== -1) {
            favourites.splice(itemFavIndex,1);
            this.setState({isFavourite: false});
        } else {
            favourites.push(this.props.vendorCode);
            this.setState({isFavourite: true});
        }

        localStorage.setItem('Favourites',favourites);
    };


    componentDidMount() {
        const {favourites}=this.props;

        if (favourites.includes(this.props.vendorCode)) {
            this.setState({isFavourite: true});
        } else {
            this.setState({isFavourite: false});
        }
    };

    render() {
        const {vendorCode, title, price, color, imgUrl, toggleModal} = this.props;
        const {isFavourite} = this.state;

        return (
            <div key={vendorCode} className='product-item'>
                <div className='product-info'>
                    <h2 className='product-title'>{title}</h2>
                    <p>Price: {price}$</p>
                    <p>Color: {color}</p>
                </div>
                <img className='product-img' alt='plant' src={imgUrl}/>
                <div className='product-btns'>
                    <FavouriteIcon isFavourite={isFavourite} toggleFavorite={this.toggleFavorite}/>
                    <Button text='Add to cart' classText='btn-to-cart' onClick={()=>{toggleModal(vendorCode)}}/>
                </div>
            </div>
        );
    }
}

export default Product;