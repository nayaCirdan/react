import './ProductList.scss'
import React, {Component} from 'react';
import Product from "../Product/Product";

class ProductList extends Component {
    render() {
        const {productList, favourites, toggleModal} = this.props;

        const products = productList.map((product) => {
            return (<Product key={product.vendorcode}
                             title={product.title}
                             price={product.price}
                             color={product.color}
                             imgUrl={product.path}
                             vendorCode={product.vendorcode}
                             favourites={favourites}
                             toggleModal={toggleModal}
            />)
        });

        return (
            <div className='product-list'>
                {products}
            </div>
        );
    }
}

export default ProductList;