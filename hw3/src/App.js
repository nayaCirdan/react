import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import {withRouter} from "react-router";



const App = (props) => {
    let favouriteFromLSArr = null;
    let inCartFromLSArr = null;
    if (localStorage.getItem('Favourites')) {
        const favouriteFromLSString = localStorage.getItem('Favourites');

        favouriteFromLSArr = JSON.parse(favouriteFromLSString)
    }
    if (localStorage.getItem('In Cart')) {
        const inCartFromLSString = localStorage.getItem('In Cart');
        inCartFromLSArr = JSON.parse(inCartFromLSString)
    }

    const [productList, setProductList] = useState([]);
    const [favourites] = useState(favouriteFromLSArr || []);
    const [modalIsOpened, setModalIsOpened] = useState(false);
    const [productsInCart, setProductsInCart] = useState(inCartFromLSArr || null);

    const [currentItem, setCurrentItem] = useState({});


    const toggleModal = (product) => {
        setModalIsOpened(!modalIsOpened);
        setCurrentItem(product);
    };

    const closeModal = () => {
        setModalIsOpened(!modalIsOpened);
    }

    const addToCart = () => {


        let cartToLocalStorage;

        if (productsInCart) {
            cartToLocalStorage=[...productsInCart,currentItem];
            setProductsInCart(cartToLocalStorage);
        } else {
            cartToLocalStorage=[currentItem];
            setProductsInCart([currentItem]);
        }
        localStorage.setItem('In Cart', JSON.stringify(cartToLocalStorage));
        closeModal();
    };

    const deleteFromCart = () => {
        const indexItemInCart = productsInCart.indexOf(currentItem);
        let newProductsInCart=productsInCart;

        newProductsInCart.splice(indexItemInCart, 1);
        localStorage.setItem('In Cart', JSON.stringify(newProductsInCart));
        closeModal();
        setProductsInCart(newProductsInCart);
    }

    useEffect(() => {
        axios('/product_items.json')
            .then(result => {
                setProductList(result.data)
            });
    }, [])


    const {location} = props;

    return (
        <>
            <Header/>
            <Main productList={productList}
                  favourites={favourites}
                  productsInCart={productsInCart}
                  toggleModal={toggleModal}
                  deleteFromCart={deleteFromCart}/>

            {location.pathname === '/cart' ?

                <Modal isOpened={modalIsOpened}
                       header='Delete from modal'
                       closeButton={'x'}
                       closeModal={closeModal}
                       text='Do you really want to delete this product?'
                       actions={[
                           <Button key='doNotDeleteFromCart'
                                   classText='btn-inModal btn-inModal__no'
                                   text='No'
                                   onClick={closeModal}/>,
                           <Button key='deleteFromCart'
                                   classText='btn-inModal btn-inModal__yes'
                                   text='Yes'
                                   onClick={deleteFromCart}/>
                       ]}/>
                :
                <Modal isOpened={modalIsOpened}
                       header='Add to modal'
                       closeButton={'x'}
                       closeModal={closeModal}
                       text='Do you want to add this cute plant to your cart?'
                       actions={[
                           <Button key='doNotAddToCart'
                                   classText='btn-inModal btn-inModal__no'
                                   text='No'
                                   onClick={closeModal}/>,
                           <Button key='addToCart'
                                   classText='btn-inModal btn-inModal__yes'
                                   text='Yes'
                                   onClick={addToCart}/>
                       ]}/>
            }
        </>
    )
}

export default withRouter(App);
