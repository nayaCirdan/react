import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

const Button = (props) => {

    const {classText, text, onClick} = props;

    return (
        <button className={(`btn ${classText}`)}
                onClick={onClick}>{text}</button>
    );
}


Button.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    classText: PropTypes.string.isRequired
};

export default Button;

