import React, {useState} from 'react';
// import Product from "../Product/Product";
import Product from "../Product/product func";

const Cart =(props)=> {
        const [productsInCart] = useState(props.productsInCart);

        const {history, toggleModal, favourites} = props;
        const nowPath=history.location.pathname;

        let cartProducts;
        if(productsInCart) {
            cartProducts = productsInCart.map((product) => {
                return(<Product product={product}
                                key={product.vendorCode}
                                nowPath={nowPath}
                                toggleModal={toggleModal}
                                favourites={favourites}
                />)

            })
        } else {
            cartProducts='Sorry, you have not added any products'
        }


        return (
            <div className='favorite-product-list product-list'>
                {cartProducts}
            </div>
        );
    }


export default Cart;