import React from 'react';
// import Product from "../Product/Product";
import Product from "../Product/product func";

const Favourites = (props) => {

    const {favourites, toggleModal, history} = props;
    const nowPath = history.location.pathname;

    const favouritesProducts = favourites.map((product) => {
        return (<Product product={product}
                         favourites={favourites}
                         toggleModal={toggleModal}
                         key={product.vendorCode}
                         nowPath={nowPath}
                         favouriteList/>)

    });

    return (
        <div className='favorite-product-list product-list'>
            {favouritesProducts}
        </div>
    );
}


export default Favourites;