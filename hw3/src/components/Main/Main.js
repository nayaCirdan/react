import React from 'react';
import {Route, Switch} from 'react-router-dom';
import ProductList from "../ProductList/ProductList";
import Favourites from "../Favourites/Favourites";
import Cart from '../Cart/Cart';
import './Main.scss'

const Main = (props) => {

    const {productList, favourites, toggleModal, productsInCart, deleteFromCart, updatePath} = props;

    return (
        <div className='main'>
            <div className='container'>
                <Switch>
                    <Route exact path='/' render={(props) => <ProductList {...props}
                                                                          productList={productList}
                                                                          favourites={favourites}
                                                                          toggleModal={toggleModal}
                                                                          updatePath={updatePath}/>}
                    />
                    <Route exact path='/favourites' render={(props) => <Favourites {...props}
                                                                                   favourites={favourites}
                                                                                   toggleModal={toggleModal}
                                                                                   productList={productList}
                                                                                   updatePath={updatePath}/>}
                    />
                    <Route exact path='/cart' render={(props) => <Cart {...props}
                                                                       productsInCart={productsInCart}
                                                                       deleteFromCart={deleteFromCart}
                                                                       updatePath={updatePath}
                                                                       toggleModal={toggleModal}
                                                                       favourites={favourites}/>}/>
                </Switch>
            </div>
        </div>
    );
}


export default Main;