import './Product.scss'
import React, {Component} from 'react';
import Button from "../Button/Button";
import FavouriteIcon from "../FavouriteIcon/FavouriteIcon";


class Product extends Component {
    state = {
        isFavourite: false,

    }


    toggleFavourite = () => {
        const {favourites, product, favouriteList} = this.props;
        debugger;
        if (this.state.isFavourite) {
            //Если уже в избранном
            const productIndexInFav = favourites.indexOf(product); //найти индекс в избранном этого товара
            if (productIndexInFav !== -1) { //если такой товар есть в избранном
                favourites.splice(productIndexInFav, 1); //удалить элемент из избранного по индексу один элеменнт
                this.setState({isFavourite: false})  // и поставить стейт не избранный
            } else {
            }
            if (favouriteList) {
                this.setState({
                    removedFromFav: true
                })

            }
        } else {
            favourites.push(product); //если еще нет в избранном, добавить в конец
            this.setState({isFavourite: true}) // и поставить стейт в избранное

        }
        localStorage.setItem('Favourites', JSON.stringify(favourites)); //локалсторедж заменить а новый массив но строокй
    }


    componentDidMount()
    {

        const {favourites, product} = this.props;

        if (favourites) {
            const currentProductInFav = favourites.find((favProduct) => {
                if (favProduct.vendorCode === product.vendorCode) {
                    return favProduct;
                }
            });
            if (currentProductInFav) {
                this.setState({isFavourite: true});
            } else {
                this.setState({isFavourite: false});
            }
        }
    }
    render()
    {

        const {title, price, color, imgUrl} = this.props.product;
        const {toggleModal, product, nowPath} = this.props;

        if (this.state.removedFromFav) {
            return false
        }
        return (
            <div className='product-item'>
                <div className='product-info'>
                    <h2 className='product-title'>{title}</h2>
                    <p>Price: {price}$</p>
                    <p>Color: {color}</p>
                </div>
                <img className='product-img' alt='plant' src={imgUrl}/>
                <div className='product-btns'>
                    <FavouriteIcon isFavourite={this.state.isFavourite}
                                   toggleFavourite={this.toggleFavourite}/>

                    {(nowPath === '/cart') ?
                        <Button text='x'
                                classText='btn-delete-from-cart'
                                onClick={() => {
                                    toggleModal(product)
                                }}/>
                        :
                        <Button text='Add to cart'
                                classText='btn-to-cart'
                                onClick={() => {
                                    toggleModal(product)
                                }}/>
                    }
                </div>
            </div>
        );
    }
}

export default Product;