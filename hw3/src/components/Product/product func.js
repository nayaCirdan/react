import './Product.scss'
import React, {useState} from 'react';
import Button from "../Button/Button";
import FavouriteIcon from "../FavouriteIcon/FavouriteIcon";

const Product = (props) => {
    const isProductFav = () => {
        const {favourites, product} = props;
        if (favourites) {
            return favourites.find((favProduct) => {
                if (favProduct.vendorCode === product.vendorCode) {
                    return favProduct;
                } else {
                    return false;
                }
            });
        }
    };

    const [isFavourite, setIsFavourite] = useState(isProductFav());
    const [removedFromFav, setRemovedFromFav] = useState(false);

    const toggleFavourite = () => {
        const {favourites, product, favouriteList} = props;

        setRemovedFromFav(function () {
            if (favouriteList) {
                return true
            }
        })

        if (isFavourite) {

            const productIndexInFav = favourites.indexOf(product);//если такой товар есть в избранном
            favourites.splice(productIndexInFav, 1); //удалить элемент из избранного по индексу один элеменнт
            setIsFavourite(false)  // и поставить стейт не избранный

        } else {

            favourites.push(product); //если еще нет в избранном, добавить в конец
            setIsFavourite(true) // и поставить стейт в избранное
        }
        localStorage.setItem('Favourites', JSON.stringify(favourites));
    }

    const setIconFav = function () {

        const {favourites, product} = props;

        if (favourites) {
            return favourites.find((favProduct) => {
                if (favProduct.vendorCode === product.vendorCode) {
                    return favProduct;
                } else {
                    return false;
                }
            });
        }
    }


    const {title, price, color, imgUrl} = props.product;
    const {toggleModal, nowPath, product} = props;

    if (removedFromFav) {
        return false
    }
    return (
        <div className='product-item'>
            <div className='product-info'>
                <h2 className='product-title'>{title}</h2>
                <p>Price: {price}$</p>
                <p>Color: {color}</p>
            </div>
            <img className='product-img' alt='plant' src={imgUrl}/>
            <div className='product-btns'>
                <FavouriteIcon isFavourite={setIconFav()}
                               toggleFavourite={toggleFavourite}/>

                {(nowPath === '/cart') ?
                    <Button text='x'
                            classText='btn-delete-from-cart'
                            onClick={() => {
                                toggleModal(product)
                            }}/>
                    :
                    <Button text='Add to cart'
                            classText='btn-to-cart'
                            onClick={() => {
                                toggleModal(product)
                            }}/>
                }
            </div>
        </div>
    );
}

export default Product;