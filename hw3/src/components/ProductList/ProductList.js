import './ProductList.scss'
import React from 'react';
// import Product from "../Product/Product";
import Product from "../Product/product func";

const ProductList =(props)=> {
        const {productList, favourites, toggleModal, history} = props;
        const nowPath=history.location.pathname;
        const products = productList.map((product) => {
            return (<Product product={product}
                             favourites={favourites}
                             toggleModal={toggleModal}
                             key={product.vendorCode}
                             productList
                             nowPath={nowPath}
            />)
        });

        return (
            <div className='product-list'>
                {products}
            </div>
        );
    }


export default ProductList;