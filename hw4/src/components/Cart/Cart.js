import React from 'react';
import Product from "../Product/Product";
import {connect} from "react-redux";
import CartTotal from "../CartTotal/CartTotal";
import ProductInCart from "../ProductInCart/ProductInCart";
import './Cart.scss'


const Cart =(props)=> {

        const {history, productList} = props;
        const nowPath=history.location.pathname;
        const productsInCart=productList.filter((product)=>product.cartCounter);
        let cartProducts;
        if(productsInCart.length) {
            cartProducts = productsInCart.map((product) => {
                return(<ProductInCart product={product}
                                key={product.vendorCode}
                                nowPath={nowPath}

                />)

            })
        } else {
            cartProducts='The cart is empty. Add some products, please'
        }


        return (
            <div className='cart-product-list product-list'>
                {cartProducts}
                <CartTotal/>
            </div>
        );
    }
const mapStoreToProps=(store)=>{
    return{
        productList: store.productList,
    }
}

export default connect(mapStoreToProps)(Cart);