import React, {Component} from 'react';
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    constructor (props) {
        super(props);

        this.state={
            modalFirst: {
                isOpened:false
            },
            modalSecond: {
              isOpened: false
            }
        };

    }


    toggleModalFirst = ()=> this.setState({modalFirst:{isOpened:!this.state.modalFirst.isOpened}});
    toggleModalSecond = ()=> this.setState({modalSecond:{isOpened:!this.state.modalSecond.isOpened}});

    closeModalFirst = () => this.setState({modalFirst:{isOpened:!this.state.modalFirst.isOpened}});
    closeModalSecond = () => this.setState({modalSecond:{isOpened:!this.state.modalSecond.isOpened}});

    render() {

        return (
            <>
                <Button backgroundColor='btn-first' text='Open first modal' onClick={this.toggleModalFirst}/>
                <Button backgroundColor='btn-second' text='Open second modal' onClick={this.toggleModalSecond}/>
                <Modal isOpened = {this.state.modalFirst.isOpened} header='First modal' closeButton={'x'} closeModal={this.closeModalFirst} text='It is first modal awesome text' actions={[
                    <Button key='mod1btn1'  backgroundColor='btn-inModal' text='I understood'/>,
                    <Button key='mod1btn2'  backgroundColor='btn-inModal' text='Oohh really?'/>
                ]}/>


                <Modal isOpened = {this.state.modalSecond.isOpened} header='Second modal' closeButton={false} closeModal={this.closeModalSecond} text='It is second modal awesome text' actions={[
                    <Button key='mod2btn1' backgroundColor='btn-inModal' text='Now i see'/>,
                    <Button key='mod2btn2' backgroundColor='btn-inModal' text='U r genius'/>

                ]}/>
            </>
        )
    }
}

export default App;
