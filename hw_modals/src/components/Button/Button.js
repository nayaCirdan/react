import React, {Component} from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends Component {
    render() {
        const {backgroundColor, text, onClick} = this.props;
        return (
            <button className={(`btn ${backgroundColor}`)} onClick={onClick}>{text}</button>
        );
    }
}

Button.propTypes={
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string.isRequired
};

export default Button;

